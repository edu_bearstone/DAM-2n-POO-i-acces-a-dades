package robots;

public class Mecanic extends MarkJava {

	@Override
	public boolean decideixSiMou() {
		if (super.decideixSiMou()) {
			if (obteEnergia() < 5) {
				recarregaBateria();
				return false;	
			}
			if (random.nextInt(10)+1 <= 6)
				return true;
		}
		return false;
	}

	@Override
	public void interactua(MarkJava unAltreRobot) {
		if (obteEnergia() > 4) {
			unAltreRobot.recarregaBateria();
			gastaEnergia(1);	
		}		
	}

}
