package exemple_validador_xml;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class ControladorErrades implements ErrorHandler {
	@Override
	public void error(SAXParseException exception) throws SAXException {
		System.err.println("ERROR : " + exception.getMessage()); 
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		System.err.println("FATAL ERROR : " + exception.getMessage()); 

	}

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		System.err.println("WARNING : " + exception.getMessage()); 
	}
}
