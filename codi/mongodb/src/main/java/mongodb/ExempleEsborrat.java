package mongodb;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gt;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExempleEsborrat {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("punts");
		
		coll.drop();
		
		for (int x=0; x<5; x++) {
			for (int y=0; y<5; y++) {
				coll.insertOne(new Document("x",x).append("y", y));
			}
		}
		
		// esborra tots els elements que compleixen la condició
		coll.deleteMany(gt("x", 2));
		// esborra el primer element que compleix la condició
		coll.deleteOne(eq("y", 1));
		
		List<Document> all = coll.find().into(new ArrayList<Document>());
		for (Document doc : all) {
			System.out.println(doc.toJson());
		}
		System.out.println();
		
		client.close();

	}

}
