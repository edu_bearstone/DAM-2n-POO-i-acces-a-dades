## Respresentació de documents

Com que el JSON no és un format natiu de Java, cal saber com es fa per
traduir un document JSON al conjunt d'objectes que el representaran.

Les classes necessàries les defineix el driver de MongoDB, i es poden utilitzar
per definir documents JSON, sense que necessàriament provinguin o s'hagin de
guardar en una col·lecció de MongoDB.

La classe principal que utilitzarem és **Document** (hi ha altres classes molt
relacionades, però *Document* és la que s'utilitza habitualment). La idea és
que cada vegada que en JSON definiríem un document amb {}, en Java això es
tradueix amb un `new Document()`.

*Document* té dos mètodes esencials per afegir parelles de camp-valor: *put*
i *append*. La diferència és que *append* retorna el propi document, amb la
qual cosa podem encadenar diverses crides, i *put* no.

Els arrays JSON es representen en Java com a llistes. El mètode
*Arrays.asList* ens és útil per transformar una enumeració de valors o un
array a una llista.

Anem a posar en pràctica això traduïnt el següent document a Java:

```json
{
  "nom" : "pere",
  "edat" : 28,
  "interessos" : ["bàsquet", "vídeojocs"],
  "telèfon" : {
    "mòbil" : "625121212",
    "fix" : "931234567"
  },
  "actiu" : true,
  "data_alta" : {
    "$date" : 1271023200000
  }
}
```

Aquest codi crea exactament el mateix document que el fitxer JSON anterior.

```java
public class ExempleDocument {

	public static void main(String[] args) {
		Document document = new Document()
				.append("nom", "pere")
				.append("edat", 28)
				.append("interessos", Arrays.asList("bàsquet", "vídeojocs"))
				.append("telèfon", new Document("mòbil", "625121212")
						.append("fix", "931234567"))
				.append("actiu", true);

		LocalDate ld = LocalDate.of(2010, 04, 12);
		Date date = Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
		document.put("data_alta", date);

		JsonWriterSettings settings = new JsonWriterSettings(true);
		System.out.println(document.toJson(settings));

		Date d = document.get("data_alta", Date.class);
		System.out.println(d);
	}

}
```
