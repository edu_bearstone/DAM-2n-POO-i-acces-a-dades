#### Abstracció

Pensar la solució d'un problema real és complex, doncs cal tenir en
compte múltiples detalls d'implementació simultàniament. Per ajudar a
superar aquesta dificultat, els problemes s'analitzen a diversos nivells
d'abstracció. Així, primer s'identifiquen quins objectes intervenen a la
resolució, després es defineixen les seves responsabilitats, que
s'acabaran traduint als seus mètodes, i finalment, s'aborda cadascuna
d'aquestes responsabilitats, completant els detalls necessaris.

L'abstracció ens permet centrar l'atenció a un nivell de detall i un
aspecte concret de la solució obviant la resta.

 * Podem analitzar el problema a **nivell conceptual**. En aquest nivell,
 ***els objectes s'entenen com un conjunt de responsabilitats***, sense
 associar-los a una implementació concreta.

 * El següent nivell d'abstracció és el **nivell d'especificació**, on entenem
 que ***un objecte és un conjunt de mètodes*** que els altres objectes poden
 cridar.

 * Finalment, en el **nivell d'implementació**, ***un objecte és un codi concret
 conjuntament a unes dades***, i les interaccions entre ells.

Imaginem un problema concret. Volem que l'usuari del nostre programa
pugui establir un criteri de cerca (per nom, descripció, tipus,
disponibilitat, preu...), a partir del qual s'obtindrà la llista de
productes que compleixen aquests criteris d'un fitxer o d'una base de
dades, i generarà un informe, que es pot mostrar per pantalla, o es pot
escriure en un fitxer PDF o un fitxer HTML.

Si comencem a pensar en detalls ens podem perdre fàcilment: quin format
tindrà el fitxer HTML, quina base de dades utilitzarem, com dissenyarem
la interfície gràfica, quins criteris es podran utilitzar... Masses
decisions a prendre massa aviat. A més, prendre aquestes decisions ara
pot tenir conseqüències molt negatives més endavant: per exemple, podria
passar que dissenyéssim el programa per generar PDF i HTML, i que més
endavant se'ns demani que es generin fitxers de text pla, o fitxers de
Word, o de Writer, o....

El que sí que tenim clar és que hi haurà:

 * Un objecte que s'encarregarà d'accedir a la base de dades.
 * Un objecte que s'encarregarà d'escriure les dades a algun tipus de fitxer.
 * Un objecte que mostrarà un formulari a l'usuari per recollir les dades.
 * Un objecte que representarà el criteri de cerca triat.

Ens estem movent en el *nivell conceptual*: hem identificat els objectes
que hi ha i les seves responsabilitats.

A continuació, passaríem al *nivell d'especificació*, i podríem definir
el comportament de cadascun dels objectes. Per exemple:

 * L'objecte que accedeix a la base de dades haurà d'establir una connexió, rebre el criteri desitjat, fer la consulta, i retornar els resultats obtinguts.

 * L'objecte que ha d'escriure les dades haurà de rebre una sèrie de registres, possiblement ordenar-los, crear un fitxer, escriure-hi les dades, i finalment tancar el fitxer.

Fixeu-vos que ara ja ens estem centrant en aspectes concrets del
programa, però encara no estem prenent masses decisions concretes.

Finalment, podem passar a *implementar* els detalls concrets. Veuríem
com s'ha de crear un fitxer PDF i quin format exacte li volem donar.

Si ho hem fet així, per una banda ens ha estat més senzill plantejar el
problema, perquè hem pogut analitzar cadascuna de les seves parts
independentment de les altres. Per altra banda, el nostre programa
s'adaptarà molt millor als canvis, perquè les implementacions concretes
s'han separat de l'esquema conceptual. Si, per exemple, ara necessitem
accedir a un gestor de bases de dades diferent, o necessitem generar un
nou tipus de fitxer, només cal que implementem aquesta part nova, que
s'acoblarà correctament amb la resta perquè no ens cal modificar
l'esquema de l'algorisme principal.
