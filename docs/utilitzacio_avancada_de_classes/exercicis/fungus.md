## Fungus

En aquest exercici desenvoluparem un cultiu de fongs. En aquest cultiu hi
crearem tres colònies de tres tipus de fongs, que s'aniran extenent amb el
temps. Quin serà el fong més exitós?

A l'enunciat s'especifiquen les classes i interfícies que hi ha, així com
els seus atributs i mètodes principals. Cal afegir els mètodes *get* i
*set* que es considerin oportuns, així com els modificadors *private*,
*public*, *protected*, *abstract*, *static* i *final*.

### Exemple d'execució

```
..........
..x.......
....O.....
..........
..........
..........
......+...
..........
..........
..........

xxxxxOOOOO
xxxxxxOOOO
xxxxxOOOOO
+xxOOOOOO+
.xOOOOOOOO
.++.OOOOO.
..+..O+OO+
+.....OOO+
....+...+.
..........

Colònia compactus, població: 39
Colònia agresibus, població: 19
Colònia dispersus, població: 11
```

### Interfícies *Arrencable* i *Arrencador*

Són dues interfícies que permetran marcar quins fongs són fàcils d'arrencar
i quins són capaços de posar-se en llocs ocupats per altres fongs. No tenen
mètodes.

### Classe *Colonia*

Tots els fongs pertanyen a una colònia. Aquesta classe facilita recomptar
quants fongs hi ha que pertanyin a la mateixa colònia.

#### Atributs

- *nom*: el nom que li posem a la colònia.

- *poblacio*: la quantitat de fongs que pertanyen a aquesta colònia.

#### Mètodes

- constructor: el constructor rep el nom de la colònia.

- *augmentaPoblacio* i *disminueixPoblacio*: augmenta o disminueix la població
en 1.

- *compareTo*: *Colonia* implementa la interfície *Comparable*. Dues colònies
es poden comparar segons la població que té cadascuna.

- *toString*: retorna una cadena amb informació de la colònia. Veure l'exemple
d'execució pel format.

### Classe *Fungus*

Aquesta classe conté les dades i mètodes comuns a tots els tipus de fongs.

#### Atributs

- *colonia*: la colònia a la qual pertany aquest fong.

- *fila* i *col*: la posició que ocupa aquest fong al cultiu.

#### Mètodes

- constructor: el constructor rep la colònia a la qual pertany.

- *getChar*: cada tipus de fong retornarà un caràcter diferent, que servirà
per visualitzar el cultiu per pantalla.

- *creix*: aquest mètode s'utilitzarà per fer que els fongs creixin. Cada
tipus de fong ho farà d'una forma diferent, però la idea és que es
crearan nous fongs del mateix tipus i que pertanyeran a la mateixa
colònia. Aquest mètode rep el cultiu on es troba el fong.

- *posa*: rep el cultiu on és i una posició (fila i columna). Retornarà
*true* si s'ha posat un nou fong o *false* si no s'ha pogut. Per poder posar
un nou fong cal comprovar que les coordenades són vàlides (són dins dels
marges del cultiu), i que en aquella posició no hi hagi ja un altre fong.
Alternativament, si hi ha un fong però és *Arrencable* i aquest tipus de fong
és *Arrencador*, també s'hi pot posar.

### Classe *Compactus*

Es tracta d'un tipus de fong que creix en grups compactes. El representarem
amb la lletra 'O'.

#### Mètodes

- *creix*: aquest fong creix, a partir de la posició que ocupa, cap a totes
les posicions adjacents que estiguin lliures.

### Classe *Dispersus*

És un tipus de fong que es pot reproduir directament a qualsevol lloc del
cultiu. A més, és *Arrencador*, és a dir, pot crèixer a posicions que ja
estaven ocupades per alguns dels altres fongs. El representarem amb el
símbol '+'.

#### Mètodes

- *creix*: quan aquest fong creix ho fa a quatre posicions aleatòries del
cultiu.

### Classe *Agresibus*

És un tipus de fong molt agresiu, que pot crèixer a sobre de **qualsevol**
altre fong. A canvi, però, és *Arrencable*, és a dir, és fàcil que altres
fongs li puguin crèixer a sobre. El representarem amb la lletra 'x'.

#### Mètodes

- *creix*: a partir de la posició que ocupa, s'extén cap a totes les posicions
veïnes, sense diagonals (amunt, avall, dreta, esquerra).

- *posa*: a diferència dels altres tipus de fongs, aquest fong sempre es pot
posar en una posició que estigui ocupada per un altre fong.

### Classe *Cultiu*

Un cultiu contindrà una matriu de fongs.

#### Atributs

- *terreny*: la matriu de fongs. Cada posició pot contenir un fong o *null*.

#### Mètodes

- constructor: el constructor rebrà les mides del *terreny*.

- *setFungus*: rebrà un fong i una posició (fila i columna). Posarà el fong a la
posició indicada (això implica posar el fong al *terreny* i actualitzar els
atributs *fila* i *col* del fong). Actualitzarà la població de cada colònia:
afegirà 1 a la població de la colònia del fong que ha posat, i, si ja hi havia
un fong en aquesta posició, haurà de restar 1 a la població de la seva colònia.

- *temps*: tria una posició aleatòria del cultiu. Si allà hi ha un fong, el
fa crèixer.

- *dibuixa*: mostra el cultiu per pantalla. Per a cada posició, si hi ha un fong
mostra el seu símbol, si no, mostra un '.'.

- *esDins*: rep una fila i columna i retorna *true* si aquestes coordenades són
vàlides o *false* si no.

### Classe *Main*

El mètode *main* farà les següents operacions:

1- Crearà tres colònies anomenades "compactus", "dispersus" i "agresibus".

2- Crearà un cultiu de 10x10.

3- Afegirà un fong de cada tipus a alguna posició del cultiu. Cada fong ha de
pertànyer a la colònia corresponent.

4- Mostrarà l'aspecte del cultiu a l'inici, farà passar 100 vegades el temps i
mostrarà l'aspecte del cultiu al final.

5- Ordenarà les colònies de més a menys població i mostrarà la seva informació
per pantalla. **Pista**: *Collections.reverseOrder()* retorna un *Comparator*
per ordenar en l'odre natural, però de major a menor en comptes de de menor a
major.
