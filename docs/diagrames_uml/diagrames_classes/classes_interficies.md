## Classes i interfícies

Les classes i interfícies es representen amb una caixa que conté el seu nom.
Opcionalment poden aparèixer els seus atributs i mètodes.

Exemple de classe, classe abstracte i interfície:

![Exemple de classe](docs/diagrames_uml/imatges/exemple_classe.png)

Cal notar que en els diagrames de classes la major part dels elements són
opcionals. Així, poden aparèixer o no els mètodes i atributs, o només una part
d'ells. També es poden incloure o no els modificadors d'accés o els tipus de
paràmetres i retorn.

En general, l'objectiu d'un diagrama de classes és representar la idea d'un
disseny orientat a objectes i es destaquen només aquells elements que
interessen. En poques ocasions es creen diagrames que representin tots els
elements d'un programa.
